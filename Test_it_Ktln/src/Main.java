// An abstract class with constructor 
abstract class Vehicle {
    private int numberOfWheels = 4;

    Vehicle() { System.out.println("Vehicle Constructor Called"); }
    abstract void ride();

    int getNumberOfWheels() {
        return numberOfWheels;
    }
}

class Kamaz extends Vehicle {
    int kamazNumberOfWheels;

    Kamaz(int kamazNumberOfWheels) {
        this.kamazNumberOfWheels = getNumberOfWheels() + kamazNumberOfWheels;
        System.out.println("Kamaz Constructor Called");
    }
    void ride() { System.out.println("Kamaz ride() called"); }
}

class BMW extends Vehicle {
    int bmwNumberOfWheels;

    BMW() {
        bmwNumberOfWheels = getNumberOfWheels();
        System.out.println("BMW Constructor Called");
    }

    void ride() { System.out.println("BMW ride() called"); }
}

class Main {
    public static void main(String[] args) {
        Kamaz kamaz = new Kamaz(10);
        BMW bmw = new BMW();

        System.out.println("Kamaz number of wheels " + kamaz.kamazNumberOfWheels);
        System.out.println("BMW number of wheels " + bmw.bmwNumberOfWheels);

    }
} 