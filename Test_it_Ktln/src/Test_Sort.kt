import java.util.*

private val stack = Stack<Int>()

fun main(args: Array<String>) {
    stack.push(20)
    stack.push(0)
    stack.push(10)
    stack.push(2)
    stack.push(5)
    stack.push(16)

    sortStack()
    println(stack)
}

private fun sortStack() {
    if (!stack.empty()) {
        val element = stack.pop()
        sortStack()
        sortedInsert(element)
    }
}

private fun sortedInsert(x: Int) {
    if (stack.empty() || stack.peek() > x) {
        stack.push(x)
    } else {
        val element = stack.pop()
        sortedInsert(x)
        stack.push(element)
    }
}
