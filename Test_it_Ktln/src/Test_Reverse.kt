import java.util.*

private val stack = Stack<Any>()

fun main(args: Array<String>) {
    stack.push('1')
    stack.push('2')
    stack.push('3')
    stack.push('4')
    stack.push(500)
    stack.push("zhopa")

    println("Original Stack")

    println(stack)

    reverse()

    println("Reversed Stack")

    println(stack)
}

private fun reverse() {
    if (stack.size > 0) {
        val element = stack.pop()
        reverse()
        insertToBottom(element)
    }
}

private fun insertToBottom(x: Any) {
    if (stack.isEmpty()) {
        stack.push(x)
    } else {
        val element = stack.pop()
        insertToBottom(x)
        stack.push(element)
    }
}