import java.util.Scanner;
import java.util.Stack;

public class MaxStack {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        Stack<Integer> maxStack = new Stack<>();
        for (int i = 0; i < n; i++) {
            String command = in.next();
            switch (command) {
                case "push":
                    int value = in.nextInt();
                    int currentMax = maxStack.isEmpty() ? Integer.MIN_VALUE : maxStack.peek();
                    maxStack.push(Integer.max(currentMax, value));
                    break;
                case "max":
                    System.out.println(maxStack.peek());
                    break;
                case "pop":
                    maxStack.pop();
                    break;
            }
        }
    }
}