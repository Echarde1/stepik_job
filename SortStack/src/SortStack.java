import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Stack;

public class SortStack {
    public static void main(String[] args) throws IOException {
        BufferedReader input = new BufferedReader(new FileReader("/home/novoselov/JAVA/Projects/Stack/SortStack/src/input.txt"));
        int n = Integer.valueOf(input.readLine());
        int[] data = Arrays.stream(input.readLine().split(" ")).mapToInt(Integer::parseInt).toArray();

        initStackAndBegin(data);
    }

    private static void initStackAndBegin(int[] data) {
        Stack<Integer> stack = new Stack<>();
        for (int element : data) {
            stack.push(element);
        }

        sortStack(stack);
        while (!stack.isEmpty()) {
            System.out.print(stack.pop() + " ");
        }
    }

    private static void sortStack(Stack<Integer> stack) {
        if (!stack.empty()) {
            int element = stack.pop();
            sortStack(stack);
            sortedInsert(stack, element);
        }
    }

    private static void sortedInsert(Stack<Integer> stack, int element) {
        if (stack.empty() || element < stack.peek()) {
            stack.push(element);
        } else {
            int tempElement = stack.pop();
            sortedInsert(stack, element);
            stack.push(tempElement);
        }
    }
}
