import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;

public class Brackets_3_Types {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        Check check = new Check(reader.readLine());
        int a = check.makeCheck();

        if (a == 0) {
            System.out.println("Success");
        } else System.out.println(a);
    }
}

class Check {
    private String input;
    private int lengthInput;

    Check(String input) {
        this.input = input;
        this.lengthInput = input.length();
    }

    int makeCheck() {
        Stack<Integer> indexes = new Stack<>(); // Indexes storage
        Stack<Character> openBrackets = new Stack<>(); // Opening brackets storage
        int i = 0;
        for (; i < lengthInput; i++) {
            char ch = input.charAt(i);
            switch (ch) {
                case '{':
                case '[':
                case '(':
                    openBrackets.push(ch);
                    indexes.push(i + 1);
                    break;
                case '}':
                case ']':
                case ')':
                    // pop and check
                    if (!openBrackets.isEmpty()) {
                        int chClosed = openBrackets.pop();
                        indexes.pop();
                        if ((ch == '}' && chClosed != '{')
                                || (ch == ']' && chClosed != '[')
                                || (ch == ')' && chClosed != '('))
                            return i + 1;
                    } else                                                 // prematurely empty
                        return i + 1;
                    break;
            }
        }
        // All brackets in string were processed. Final check
        if (!openBrackets.isEmpty()) {
            if (openBrackets.peek() == '{' || openBrackets.peek() == '(' || openBrackets.peek() == '[') {
                return indexes.peek();
            }
            return i + 1;
        }
        return 0;
    }
}