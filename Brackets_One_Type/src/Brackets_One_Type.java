import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Stack;

public class Brackets_One_Type {
    public static void main(String[] args) throws IOException {
        BufferedReader input = new BufferedReader(new FileReader("/home/novoselov/JAVA/Projects/Stack/Brackets_One_Type/src/input.txt"));
        String str = input.readLine();

        checkBrackets(str);
    }

    private static void checkBrackets(String str) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < str.length(); i++) {
            char bracket = str.charAt(i);

            if (bracket == '(') {
                stack.push(bracket);
                continue;
            }

            if (stack.isEmpty() && bracket == ')') {
                System.out.println("Все плохо");
                return;
            } else {
                stack.pop();
            }
        }

        if (!stack.isEmpty()) {
            System.out.println("Все плохо");
        } else {
            System.out.println("Все хорошо");
        }
    }
}
